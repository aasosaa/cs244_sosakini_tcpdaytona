import signal, os
import subprocess
import Queue
import argparse
from datetime import datetime

GLOBAL_SPORT = 3000
GLOBAL_DPORT = 8000
GLOBAL_FILE_NAME = 'tcpdump_run'
OUTPUT_FILE = None

def cleanup(signum, frame):
    print("SIGINT/SIGTERM caught... closing file and exiting...")
    OUTPUT_FILE.close()

def main():
    parser = argparse.ArgumentParser(description='Watch TCPDump traffic between two ports.')
    parser.add_argument('--source_port', type=int, default=0, help='port of the host sending packets')
    parser.add_argument('--dest_port', type=int, default=0, help='port of the host receiving packets')
    parser.add_argument('--filename', help='the name of the output file')

    args = parser.parse_args()

    global GLOBAL_SPORT
    global GLOBAL_DPORT
    if args.source_port != 0:
        GLOBAL_SPORT = args.source_port
        
    if args.dest_port != 0:
        GLOBAL_DPORT = args.dest_port
        
    signal.signal(signal.SIGINT, cleanup)
    signal.signal(signal.SIGTERM, cleanup)
    
    global OUTPUT_FILE
    if args.filename != None:
        OUTPUT_FILE = open(args.filename, 'w')
    else:
        OUTPUT_FILE = open(GLOBAL_FILE_NAME, 'w')
    
    # Run tcpdump and parse
    print("Running tcpdump...")
    tcpdump = subprocess.Popen(('sudo', 'tcpdump', '-i', 'any', '-l', '-tttt', 'src', 'port', str(GLOBAL_DPORT)), stdout=subprocess.PIPE)
    first_packet_time = None
    last_seen_time = None
    # For each output row...
    for row in iter(tcpdump.stdout.readline, b''):
        print(row)
        if row == '\n':
            break

        # ...grab the timestamp, compute the arrival time, and output it with its seqno.
        timestamp = row[:26]
        _, seq, _ = row.split(',', 2)
        seq = seq.lstrip()
        if (":" in seq) and ("seq" in seq):
            _, seq = seq.split(':', 1) 
        else:
            seq = None
        last_seen_time = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')
        if first_packet_time == None:
            first_packet_time = last_seen_time
        else:
            secs_differences = (last_seen_time - first_packet_time).total_seconds()
            #print(secs_differences)
            if (seq != None):
                OUTPUT_FILE.write(str(secs_differences) + ',' + seq + '\n')
                OUTPUT_FILE.flush()
            
    print("Done!")

if __name__ == "__main__":
    main()
