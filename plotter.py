import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def getMapFromFile(filename):
    xs = []
    ys = []
    with open(filename) as fp:
        for line in fp:
            params = line.split(",")
            xs.append(params[0])
            ys.append(params[1])
    return xs, ys

def plot_data(normal_file, exploit_file, save_file, no_show):
    normalX, normalY = getMapFromFile(normal_file)
    exploitX, exploitY = getMapFromFile(exploit_file)

    exploit_name = exploit_file.split(".")[0]

    print(exploit_name)
    p1 = plt.scatter(normalX, normalY, color='r', marker='o', label='Non-malicious Receiver')
    p2 =plt.scatter(exploitX, exploitY, color='b', marker='x', label=exploit_name+' Receiver')
    plt.title("Packet arrival times for Non-malicious and "+exploit_name + " Receivers")
    plt.xlabel("Time (s)")
    plt.ylabel("Sequence number of packet arrival")
    plt.legend(handles=[p1,p2], loc =4)
    if no_show == False:
        plt.show()

    if save_file != '':
        plt.savefig(save_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot data.')
 
    parser.add_argument('--normal_file', default="normalTCPDump.txt", help='Filename of packet timings of unmodified TCP')
    parser.add_argument('--exploit_file',required=True, help='Filename of packet timings of exploited TCP')
    parser.add_argument('--save_file', default="", help='Filename to write plot to.')
    parser.add_argument('--no_show', action='store_true', help='If this flag is used, won\'t display the plot.')
    

    args = parser.parse_args()
    print(args.no_show)    
    plot_data(args.normal_file, args.exploit_file, args.save_file, args.no_show)
