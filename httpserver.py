from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

class RequestHandler(BaseHTTPRequestHandler):
    
    def do_GET(self):
        # Sends a file to the client that sent us a GET request

        print("Got a request, going to respond with a file")
        self.send_response(200)

        with open("100KBfile") as fd:
            self.wfile.write(fd.read())

        self.wfile.close()

    
    do_PUT = do_GET
    do_DELETE = do_GET
        
def main():
    port = 8000
    print('Listening on localhost:%s' % port)
    server = HTTPServer(('', port), RequestHandler)
    server.serve_forever()

        
if __name__ == "__main__":    
    main()
