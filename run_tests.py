# A crappy but working harness/driver for running our tests and plotting the results.

import os
import time
import subprocess

# This is somewhat documented by the print statements before each section.
def runner(script_name):
    print('run_tests: running script - ' + script_name)
    mininet_string = "mn --link tc,delay='20ms'"

    print('run_tests: starting child shell')
    shellForMininetProc = subprocess.Popen('/bin/sh', bufsize=1, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    print('run_tests: starting mininet')
    shellForMininetProc.stdin.write(mininet_string+'\n')
    line = ''
    while line != '*** Starting CLI:':
        line = shellForMininetProc.stdout.readline()
        line = line.rstrip()
        print(line)

    print('run_tests: Running exploit')
    f = open(script_name, 'r')
    line = ''
    line = f.readline()
    line.rstrip()
    while 'kill' not in line:
        print('mininet> ' + line)
        shellForMininetProc.stdin.write(line + '\n')
        line = f.readline()
        line.rstrip()
        time.sleep(1)
    inputLine = line

    print("run_tests: Waiting on the exploit")
    line = ''
    while line != 'Done!':
        line = shellForMininetProc.stdout.readline()
        line = line.rstrip()
        #print(line)
        
    print("run_tests: Exploit finished! Cleaning up and exiting...")
    line = inputLine
    while line != '':
        print('mininet> ' + line)
        shellForMininetProc.stdin.write(line + '\n')
        line = f.readline()
        line.rstrip()
        time.sleep(1)
        
    shellForMininetProc.stdin.write('exit' + '\n')

# Run all four tests.
runner('scripts/testNoAcks_mininet.script')
runner('scripts/testDupAcks_mininet.script')       
runner('scripts/testByteAcks_mininet.script')
runner('scripts/testOptAcks_mininet.script')

print("run_tests: Done collecting data from Mininet. Now plotting duplicate ACKs exploit vs. normal, optimistic ACKs exploit vs. normal, and byte-level ACKs exploit vs. normal.")

# Plot the output (three figures.)
subprocess.call('python plotter.py --normal_file normal.data --no_show --save_file dupAcks_plot.png --exploit_file dupAcks.data', shell=True)
subprocess.call('python plotter.py --normal_file normal.data --no_show --save_file optAcks_plot.png --exploit_file optAcks.data', shell=True)
subprocess.call('python plotter.py --normal_file normal.data --no_show --save_file byteAcks_plot.png --exploit_file byteAcks.data', shell=True)

print("run_tests: Done!")
